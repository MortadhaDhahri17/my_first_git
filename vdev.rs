use crate::bindings::* ; 

pub struct HlistHead(*mut bindings::hlist_head) ;
pub struct NetDev (*mut bindings::net_device) ; 

impl NetDev{
    
}

/// Binding alloc_netdev with alloc_netdev_mqs because why not 
/// unsafe function 
/// 
pub unsafe fn alloc_netdev (sizeof_priv:core::ffi::c_int,
    name: *const core::ffi::c_char,
    name_assign_type: core::ffi::c_uchar,
    setup: ::core::option::Option<unsafe extern "C" fn(arg1: *mut net_device)>,
    txqs: core::ffi::c_uint,
    rxqs: core::ffi::c_uint,)->  *mut bindings::net_device {
unsafe {bindings::alloc_netdev_mqs(sizeof_priv,name,name_assign_type,setup,1,1) } 
}


///Defining macro HLIST_HEAD in c : 
///macro hlist_head! 
/// Opting for using Option is also an option here 

impl HlistHead {
    fn hlist_head_macro() -> Self {
        Self {
            first: core::ptr::null(),
        }
    }
}



/// Defining macro hlist_entery from c 
/// using container_of! macro in rust 
/// 

#[macro_export]
macro_rules! hlist_entry {
    ($ptr:expr, $type:ty, $($f:ident).*) => {{
        // Use the container_of macro to calculate the address of the struct
        container_of!($ptr, $type, $($f).*)
    }};
}


/// Taking care of Null using Options in rust for the following macro 
/// hlist_entry_safe_macro 
/// hlist_entry_safe 
/*
    *Usage : 
    *let ptr: Option<*const Field> = ...; // Pointer to the field inside the struct
    *let entry_ptr: Option<*const Struct> = hlist_entry_safe!(ptr, Struct, member);
*/

#[macro_export]
macro_rules! hlist_entry_safe {
    ($ptr:expr, $type:ty, $($f:ident).*) => {{
        let ____ptr = $ptr;
        if let Some(____ptr) = ____ptr {
            Some(hlist_entry!(____ptr, $type, $($f).*))
        } else {
            None
        }
    }};
}


///  final macro hlist_for_each_entry_safe
///*


/*hlist_for_each_entry_safe!(dl, node, &ce_gw_dev_allocated, ce_gw_dev_list, list_alloc, {
    // Access dl safely, perform operations
    if let Some(dl) = unsafe { dl.as_ref() } {
        // Check if the eth_dev matches the current device in the list
        // Assuming `eth_dev` is a member of `ce_gw_dev_list`
        if eth_dev == dl.eth_dev {
            return true;
        }
    }*/

///*/  

#[macro_export]
macro_rules! hlist_for_each_entry_safe {
    ($pos:ident, $n:ident, $head:expr, $type:ty, $member:ident) => {
        let mut $pos = hlist_entry_safe!($head.first, $type, $member);
        while let Some(mut ____pos) = $pos {
            let $n = unsafe { (*____pos).$member.next };
            $pos = hlist_entry_safe!($n, $type, $member) ; c
        }
    };
}


/// Some of the exports that we are going to be needing later ! 
bool rust_helper_netif_device_present(const struct net_device *dev) {
    return netif_device_present(dev) ; 
}
EXPORT_SYMBOL_GPL(rust_helper_netif_device_present);

void rust_helper_netif_stop_queue(struct net_device *dev) {
    
    return netif_stop_queue(dev) ; 
}
EXPORT_SYMBOL_GPL(rust_helper_netif_stop_queue);

void rust_helper_hlist_add_head_rcu(struct hlist_node *n,struct hlist_head *h)
{
    return hlist_add_head_rcu(n,h) ; 
}
EXPORT_SYMBOL_GPL(rust_helper_hlist_add_head_rcu);

void *rust_helper_netdev_priv(const struct net_device *dev) {
    return netdev_priv(dev) ; 
}
EXPORT_SYMBOL_GPL(rust_helper_netdev_priv);

void rust_helper_hlist_del_rcu(struct hlist_node *n){
    return hlist_del_rcu(n); 
}
EXPORT_SYMBOL_GPL(rust_helper_hlist_del_rcu);
